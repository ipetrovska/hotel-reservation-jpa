package com.irenapetrovska.hotelreservationsystemjpa.hotel;

import com.irenapetrovska.hotelreservationsystemjpa.exception.ResourceValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HotelService {

    private final HotelRepository repository;

    public HotelDTO create(final HotelRequest request){
        var isPresentHotel= repository.findByName(request.name).isPresent();
        if(isPresentHotel){
            throw new RuntimeException("The hotel already exists.");
        }
        var hotel = new Hotel(request.name);
        return repository.save(hotel).toDTO();
    }

}
