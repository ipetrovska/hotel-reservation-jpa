package com.irenapetrovska.hotelreservationsystemjpa.hotel;

import lombok.RequiredArgsConstructor;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/hotels")
@RequiredArgsConstructor // create constructor for all final properties
public class HotelResource {

    private final HotelService service;

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public HotelDTO create(@RequestBody HotelRequest request){
        return service.create(request);
    }
}
