CREATE TABLE hotel
(
    id bigserial NOT NULL,
    name character varying(50) NOT NULL,
    CONSTRAINT hotel_pkey PRIMARY KEY (id)
);
